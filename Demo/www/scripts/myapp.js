﻿(function () {
    'use strict';

    angular.module('myapp.directives', []);
    angular.module('myapp.controllers', []);
    angular.module('myapp.services', []);

    var app = angular.module('myapp', ['ngRoute', 'ngAnimate', 'ngTouch', 'ngSanitize',
            'myapp.services', 'myapp.controllers', 'myapp.directives']);

    app.config(function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
    });

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                //title: 'My App Demo Home Page',
                templateUrl: 'views/home.html',
                controller: 'homeCtrl',
                controllerAs: 'vm'
            })
        .when('/second', {
            //title: 'My App Demo Home Page',
            templateUrl: 'views/home_2.html',
            controller: 'homeCtrl',
            controllerAs: 'vm'
        })
    }]);
})();