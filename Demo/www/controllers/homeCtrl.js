﻿(function () {

    'use strict';

    var injectParams = ["$location"];

    var homeCtrl = function ($location) {

        var vm = this;

        vm.welcome = 'Welcome to my android demo!!!';
        vm.welcome2 = 'The second view guys!!!';

        vm.ChangeRoute = function () {
            $location.path('/second');
        }
    };

    homeCtrl.$inject = injectParams;
    angular.module('myapp.controllers').controller('homeCtrl', homeCtrl);
}());